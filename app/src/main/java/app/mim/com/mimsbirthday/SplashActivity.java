package app.mim.com.mimsbirthday;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.HashMap;

public class SplashActivity extends AppCompatActivity {

    TextView title;
    TextView remainingTime;
    CountDownTimer countDownTimer;

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        initView();


        animateTitle();


        /*time calculation*/

        Helper helper = new Helper();


        SetICOTimeOut(Helper.getTimeFromMillisecond(helper.getMilliseconds()));

        TimeCount();

        if (Helper.isNow()) {

            Intent intent = new Intent(this, Dashboard.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            finish();
            startActivity(intent);


        }


        Calendar calendar = Calendar.getInstance();
        calendar.set(2018, Calendar.OCTOBER, 26, 24 ,0, 0);
        if (!(calendar.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())) {
            setAlarmTime(calendar.getTimeInMillis());
        }


    }

    private void animateTitle() {

        Animation a = AnimationUtils.loadAnimation(this, R.anim.title_animator);
        a.reset();
        title.clearAnimation();
        title.startAnimation(a);
        a.setRepeatMode(2);

    }


    private void initView() {
        title = findViewById(R.id.title);
        remainingTime = findViewById(R.id.remain_time);

    }


    private void TimeCount() {
        final Helper helper = new Helper();
        countDownTimer = new CountDownTimer(helper.getMilliseconds(), 1000) {
            @Override
            public void onTick(long l) {
                /*call this method every one second with reducing 1000 millisecond */
                SetICOTimeOut(Helper.getTimeFromMillisecond(l));
            }

            @Override
            public void onFinish() {
                /*code to execute after the countdown finish*/
            }
        }.start();
    }

    @SuppressLint("DefaultLocale")
    private void SetICOTimeOut(HashMap<String, Integer> timeFromMillisecond) {
        remainingTime.setText(String.format("Your Birthday remaining:\n %d days %d hours %d min and %d seconds", timeFromMillisecond.get("day"),
                timeFromMillisecond.get("hour"),
                timeFromMillisecond.get("min"), timeFromMillisecond.get("sec")));
    }


    private void setAlarmTime(long afterMilliseconds) {

        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent intent = new Intent(this, MyReceiver.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        if (manager != null) {

            manager.set(AlarmManager.RTC_WAKEUP, afterMilliseconds, pendingIntent);

        }

       // Toast.makeText(this, "Alarm Set Success!", Toast.LENGTH_SHORT).show();
    }
}
