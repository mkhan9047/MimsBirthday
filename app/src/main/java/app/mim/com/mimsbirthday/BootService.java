package app.mim.com.mimsbirthday;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.Calendar;

public class BootService extends IntentService {

    public BootService() {
        super("BootService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH), Calendar.getInstance().get(Calendar.HOUR_OF_DAY), 21, 0);
        if (!(calendar.getTimeInMillis() < Calendar.getInstance().getTimeInMillis())) {
            setAlarmTime(calendar.getTimeInMillis());
        }


    }

    private void setAlarmTime(long afterMilliseconds) {

        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent intent = new Intent(this, MyReceiver.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0);

        if (manager != null) {

            manager.set(AlarmManager.RTC_WAKEUP, afterMilliseconds, pendingIntent);

        }

        Toast.makeText(this, "Alarm Set Success!", Toast.LENGTH_SHORT).show();
    }
}
