package app.mim.com.mimsbirthday;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class DashFragment extends Fragment {

    CardView cake, gallery, speech, gift;


    public DashFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dash, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        View view = getView();

        if (view != null) {

            cake = view.findViewById(R.id.cake);
            gallery = view.findViewById(R.id.gallery);
            speech = view.findViewById(R.id.speech);
            gift = view.findViewById(R.id.gift);
        }

        final Activity activity = getActivity();
        if (activity != null && isAdded()) {
            cake.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Dashboard) activity).fragmentTransaction(new CakeFragment());
                }
            });
            gallery.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Dashboard) activity).fragmentTransaction(new Gallery_fragment());
                }
            });
            speech.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Dashboard) activity).fragmentTransaction(new SpeechFragment());
                }
            });

            gift.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((Dashboard) activity).fragmentTransaction(new BirthdayGift());
                }
            });
        }

    }
}
