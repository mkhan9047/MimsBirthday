package app.mim.com.mimsbirthday;

import android.util.Log;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class Helper {

    public static HashMap<String, Integer> getTimeFromMillisecond(long millis) {

        /*make a placeholder hash map object*/
        HashMap<String, Integer> holder = new HashMap<>();

        /*get total days from the Milliseconds with TimeUnit Object */
        int days = (int) TimeUnit.MILLISECONDS.toDays(millis);
        /*minus the days from the total seconds*/
        millis -= TimeUnit.DAYS.toMillis(days);
        /*get total hours from the Milliseconds with TimeUnit Object */
        int hours = (int) TimeUnit.MILLISECONDS.toHours(millis);
        /*minus the hours from the total seconds*/
        millis -= TimeUnit.HOURS.toMillis(hours);
        /*get total minutes from the Milliseconds with TimeUnit Object */
        int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(millis);
        /*minus the minutes from the total seconds*/
        millis -= TimeUnit.MINUTES.toMillis(minutes);
        /*get total seconds from the Milliseconds with TimeUnit Object */
        int seconds = (int) TimeUnit.MILLISECONDS.toSeconds(millis);


        /*put the days, hours, minutes and seconds to the HashMap */
        holder.put("day", days);
        holder.put("hour", hours);
        holder.put("min", minutes);
        holder.put("sec", seconds);

        /*return the hashMap*/
        return holder;
    }


    public long getMilliseconds() {

        Calendar currentDate = Calendar.getInstance();

        Calendar birthday = Calendar.getInstance();

        currentDate.set(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH)); // January is 0, Feb is 1, etc.

        currentDate.setTimeZone(TimeZone.getDefault());

        birthday.set(2018, Calendar.OCTOBER, 26, 24, 0, 0);

        birthday.setTimeZone(TimeZone.getDefault());

       // Log.e("date", "current date = " + currentDate.getTime().toString() + "\n" + "birthday = " + birthday.getTime().toString());




// set the year, hour, minute, second, and millisecond
        long currentDateInMillis = currentDate.getTimeInMillis();

// set the month, date, year, hour, minute, second, and millisecond
        long birthrateTimeInMillis = birthday.getTimeInMillis();


        return birthrateTimeInMillis - currentDateInMillis;

    }


    public static boolean isNow(){

        Calendar currentDate = Calendar.getInstance();

        Calendar birthday = Calendar.getInstance();

        currentDate.set(Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH)); // January is 0, Feb is 1, etc.

        currentDate.setTimeZone(TimeZone.getDefault());

        birthday.set(2018, Calendar.OCTOBER, 26, 24 ,0, 0);

        birthday.setTimeZone(TimeZone.getDefault());

     //   Log.e("date", "current date = " + currentDate.getTime().toString() + "\n" + "birthday = " + birthday.getTime().toString());




// set the year, hour, minute, second, and millisecond
        long currentDateInMillis = currentDate.getTimeInMillis();

// set the month, date, year, hour, minute, second, and millisecond
        long birthrateTimeInMillis = birthday.getTimeInMillis();


        return birthrateTimeInMillis - currentDateInMillis <= 0;

    }

}
