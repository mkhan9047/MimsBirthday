package app.mim.com.mimsbirthday;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class Gallery_fragment extends Fragment {

    ViewPager viewPager;

    ImageSliderAdapter adapter;

    public Gallery_fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_gallery_fragment, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        View view = getView();

        if (view != null) {

            viewPager = view.findViewById(R.id.viewpager);


        }

        final Activity activity = getActivity();
        if (activity != null && isAdded()) {
            List<Integer> integers = new ArrayList<>();
            integers.add(R.drawable.on);
            integers.add(R.drawable.th);
            integers.add(R.drawable.tw);
            integers.add(R.drawable.fr);
            integers.add(R.drawable.fv);
            integers.add(R.drawable.sx);
            adapter = new ImageSliderAdapter(integers, getContext());


            viewPager.setAdapter(adapter);
        }

    }

}
