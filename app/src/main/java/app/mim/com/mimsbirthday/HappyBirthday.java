package app.mim.com.mimsbirthday;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

public class HappyBirthday extends AppCompatActivity {

    ImageView anim;
    FloatingActionButton button;
    private AnimationDrawable myAnimationDrawable;
    MediaPlayer mediaPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_happy_birthday);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }

        anim = findViewById(R.id.anim);
        button = findViewById(R.id.forword);


//casting the Animation Drawable
        myAnimationDrawable
                = (AnimationDrawable) anim.getBackground();

        //start the animation
        myAnimationDrawable.start();

        try {
            mediaPlayer = MediaPlayer.create(this, R.raw.song);

            mediaPlayer.start();
        } catch (Exception e) {
            Log.d("mim_error", e.getMessage());
        }


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mediaPlayer.isPlaying()) {
                    mediaPlayer.stop();
                }

                Intent intent = new Intent(HappyBirthday.this, Dashboard.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onStop() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        super.onStop();

    }

    @Override
    protected void onPause() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        super.onPause();
    }
}
