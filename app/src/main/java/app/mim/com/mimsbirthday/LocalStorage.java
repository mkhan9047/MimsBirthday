package app.mim.com.mimsbirthday;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalStorage {

    private static boolean isBirthday = false;

    Context context;

    public LocalStorage(Context context) {
        this.context = context;

    }


    private SharedPreferences.Editor getPreferencesEditor() {
        return getsharedPreferences().edit();
    }

    private SharedPreferences getsharedPreferences() {

        return context.getSharedPreferences("MyData", Context.MODE_PRIVATE);
    }

    public void setIsBirthday(boolean isBirthday) {

        getPreferencesEditor().putBoolean("birthday", isBirthday).commit();
    }

    private boolean getIsBirthday() {

        return getsharedPreferences().getBoolean("birthday", isBirthday);
    }
}

