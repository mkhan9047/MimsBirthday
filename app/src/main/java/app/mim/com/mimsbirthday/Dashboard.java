package app.mim.com.mimsbirthday;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Dashboard extends AppCompatActivity {

    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        fragmentTransaction(new DashFragment());


    }


    public void fragmentTransaction(Fragment fragment) {
        this.fragment = fragment;
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_con, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {

        if (!(fragment instanceof DashFragment)) {

            fragmentTransaction(new DashFragment());

        } else {

            moveTaskToBack(true);
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(1);
        }


    }
}
